import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

declare var webix;

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  showTextFieldToggle:boolean = false;
  tree: any;
  @ViewChild('createNodeInput') createNodeInput : ElementRef;
  @ViewChild('webixTreeContainer') webixTreeContainer : ElementRef;
  movies: any[] = [
    {id:1, title: "Seven", type: 'triller' },
    {id:2, title: "The Silence of the Lambs", type: 'triller' },
    {id:3, title: "Memento", type: 'triller' },
    {id:4, title: "Shutter Island", type: 'triller' },
    {id:5, title: "The Hangover", type: 'comedy' },
    {id:6, title: "21 Jump Street", type: 'comedy' },
    {id:7, title: "Napoleon Dynamite", type: 'comedy' },
    {id:8, title: "Anchorman: The Legend of Ron Burgundy", type: 'comedy' },
    {id:9, title: "The Other Guys", type: 'comedy' },
    {id:10, title: "Shrek", type: 'animation' },
    {id:11, title: "Moana", type: 'animation' },
    {id:12, title: "Frozen", type: 'animation' },
    {id:13, title: "Wreck-It-Ralph", type: 'animation' },
    {id:14, title: "Big Hero 6", type: 'animation' }
  ]
  constructor() { }

  ngOnInit() {
    this.tree = webix.ui({
      view: 'tree',
      container: this.webixTreeContainer.nativeElement,
      select: true,
      drag: true,
      data: this.feedTree(),
    })
  }

  add(){
    if(this.createNodeInput.nativeElement.value.trim() == '') return;
    if(this.tree.getSelectedId()){
      this.tree.add({id: this.createNodeInput.nativeElement.value, value: this.createNodeInput.nativeElement.value },0,this.tree.getSelectedId())
    }else{
      this.tree.add({id: this.createNodeInput.nativeElement.value, value: this.createNodeInput.nativeElement.value },0,0)
    }
    this.createNodeInput.nativeElement.value = '';
    this.showTextFieldToggle = !this.showTextFieldToggle;
    return false;
  }

  remove(){
    if(!this.tree.getSelectedId())return;
    if(this.tree.isBranch(this.tree.getSelectedId())){
      //  this is a tree
      let children = [];
      this.tree.data.eachLeaf(this.tree.getSelectedId(), (child)=>{
        children.push(child.id);
      })
      children.forEach(childId=>{
        this.tree.move(childId, 0, 0);
      })
      this.tree.remove(this.tree.getSelectedId());
    }else{
      this.tree.remove(this.tree.getSelectedId());
    }
  }

  feedTree(){
    // { id: any, value:any, data?: any}
    let data:any[] = [];
    let types = [];
    this.movies.forEach(movie=>{
      if(!types.includes(movie.type)){
        types.push(movie.type);
        data.push({id: movie.type, value: movie.type, data: []});
      }
    })

    this.movies.forEach(movie=>{
      if(movie.type){
        data.forEach(category=>{
          if(category.id == movie.type){
            category.data.push({id: movie.id, value: movie.title});
          }
        })
      }else{
        data.push({id: movie.id, value:movie.title })
      }
    })
    console.log(data);
    return data;
  }
}
