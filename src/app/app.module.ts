import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { DirectorsComponent } from './directors/directors.component';
import { MoviesComponent } from './movies/movies.component'

@NgModule({
  declarations: [
    AppComponent,
    DirectorsComponent,
    MoviesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot([
      {path: '', component: DirectorsComponent},
      {path: 'movies', component: MoviesComponent}
    ]),
    NgxDatatableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
